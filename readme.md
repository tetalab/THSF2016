## THSF(5+2)

La TOULOUSE HACKER SPACE FACTORY est un rendez-vous basé sur la mise en commun 
de connaissances, une rencontre autour de propositions artistiques, de 
conférences et d’ateliers de pratiques. L’objectif est de générer un espace 
critique et expérimental qui mette en porosité création artistique et pratiques 
des cultures hackers.

Depuis 2010, dans la lignée du Chaos Computer Club (Berlin), ou du /tmp/lab 
(Vitry sur Seine), la THSF met en relation sur quatre jours, plusieurs 
hackerspaces invités à se rencontrer, échanger, créer ensemble.
Le temps de travail que constitue l’événement, est organisé par et pour les 
acteurs eux mêmes, la particularité de la Toulouse HackerSpace Factory étant de
s’inscrire au sein d'un collectif d’artistes autogéré.

L’événement est tourné vers l’utilisation libre des technologies, leurs 
appropriations et les enjeux que cela suscite dans la transformation de nos 
sociétés. Il est ouvert à tout-e-s, débutant-e-s, expert-e-s ou intéressé-e-s.
C’est un temps de rencontres et d’échanges, dont la formule hybride stimule la 
réflexion et la recherche dans de nombreux domaines, artistique, technologique, 
sociologique, philosophique...

2016 sera la 7 e édition de cet événement à Mix’Art Myrys Toulouse.
Pour cette édition, nous faisons le constat que l’événement s’inscrit dans 
un courant d’actions qui émergent un peu partout en Europe et dans le monde 
autour d’une forme originale d’organisation, expérimentale et évolutive. 

[projet THSF 2016](THSF2016_projet.md)
