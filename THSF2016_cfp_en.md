### *TOULOUSE HACKER SPACE FACTORY 2016 - Call for Participation*
### May 19th-22nd 2016, Mixart`Myrys (Toulouse, France) 

This is the THSF 2016 call for participation to all hackers, performers, 
makers, social thinkers, artists, ...

Deadline for submission is April 10th 2016.


#### ABOUT THSF
The TOULOUSE HACKER SPACE FACTORY is an event based on knowledge sharing, 
meetings around artistic propositions, conferences and workshops.
This hybrid formulation aims at creating a critical and experimental space, and 
stimulates reflexion and research in various domains (artistic, technological, 
sociological, philosophic, etc).

The event is focused on free usage and reappropriation of technology, and what 
it implies on the transformation of our societies. 
It's open to all, beginners, experts and curious people.


#### THSF7
2K16 will be the 7th edition of this event taking place at [Mix’Art Myrys]
(http://www.mixart-myrys.org), in Toulouse, South of France.

A number of essential topics (monitoring military law in France , Snowden 
disclosure, free software, open data ...) were broached in an unsatisfaying way 
lately. A disappointment feeling has been largely shared this summer in the 
hacker community and even more widely since the end of year 2015. So, what's 
next? What can we do now?

Question the notion of temporality and long time of ideas to think action
appears efficient to us, when our will is that ideas continue to act and survive 
despite the context hazards, beyond individuals, groups and events.

**We are going to overhaul the notion of temporality whether**:

* Individual and biologic (psychologic, everyday routine, health),
* Politics (Greek letter's deadline, Agora 500 BC)
* Media (Available brain time for advertisments),
* War (information speed, real time UAV),
* Finance (High Frequency Trading),
* Artistic (Real time, ephemeral event),
* IT (0 day),
* Physics (t observable)
* Human (Is it Beer o'clock yet ?)


#### FACILITIES
At our disposal:
- a 4000m2 warehouse,
- spaces for workshops,
- a conference room, 
- a concert hall,
- a 250m2 room dedicated to a hacker residency during the event,
- video and audio stream,
- places for many installations (artistic, hacking project, etc),
- two or more bars,
- food at a good price,
- a huge pipe to internetz (8Gb / s),
- a parking (free)

**The place will be opened 24/24 during the 4 days of the event for every participant(s).**

#### TRANSPORT, COST AND ACCOMODATIONS
Accommodation in Toulouse, travel reimbursement, Much respect and love.
Please specify travel details and accomodation needs in the CFP proposal.
Food and drinks will be provided for participants on the day(s) they perform.

#### DATES :
CFP submission deadline: April 10th 2016

THSF Event: May 19th 22nd 2016

#### CONTACTS
CFP proposals and any questions/comments should be sent to : [contact@tetalab.org](./)

#### LAST WORDS
We believe that diversity strongly contributes to the event 

We hope to see you contribute to this event, we want **a focused event by the 
community for the community without any commercial expectation** 
This does not mean that the beer will be free :)

##### THSF is a organised by Tetalab, Tetaneutral & Mix'Art Myrys.

#### PREVIOUS EDITION PRESS (FRENCH LANGUAGE)
[Makery: Hackers meet public at Toulouse Hacker Space Factory](http://www.makery.info/2015/05/19/les-hackers-a-la-rencontre-du-grand-public-au-thsf-de-toulouse)

[Libération: Toulouse Canal du Midi surfed by hackers](http://www.liberation.fr/futurs/2015/05/15/a-toulouse-les-hackers-surfent-sur-le-canal-du-midi_1310194)