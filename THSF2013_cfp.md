========== THSF 2013 ==========
=== Call For Participations ===
=========== Tetalab ===========

THEME >>> Re(code) Re(art) Re(boot) Re(mix) Re(verse) <<<


# About THSF

Toulouse Hacker Space Factory is an annual meeting started in 2010 around conferences and workshops.
The goal is to create encounters between hackers, makers and curious people in order to learn, test or simply have fun with technologies.
/tmp/lab in Paris initiated the movement and we wanted to bring such an event to southern France.
It is an open event and everyone is welcome without financial barrier.
More than leetness we expect open minds, sharing, technology for nothing, technology for something.

# When

# 17-24 May 2013 - Workshops and meetings between participants
# 24-26 May 2013 - Public event with Workshops + Confs + Exhibits


# Topics

    RE(use) of Technology

    Minitel / Arduino / 3D Printing 

    Oldies but goodies

    Open Hardware and Open Source Software

    Network Neutrality by doing it

    DIY

    Internet Media Politics

    Video / Numeric Art

    Soldering

    Punk and/or Trashy music on stage

    Opensource GSM

    RFID

    Lockpicking

    Retrogaming

    Streaming

    Hackerspaces

    Fablab

    Collaborative edition

    Le Bit et le Couteau

    Online radio

    Open to other propositions


# Submitting Content

    Subscribe to THSF 2013 mailing-list: http://lists.tetalab.otsfh 2013rg/listinfo/thsf-2013

    Submit you content by mail: thsf-2013@lists.tetalab.org


# Speaker benefits:

Accommodation in Toulouse,
travel reimbursement,
Much respect and love.

# Deadline

We'll have to prepare 'something' for the end of january, so you'll have more chance to be included if you submit your content before this date.

-----8X---------------8X-----------8X----------------
              [ CFP ANSWER TEMPLATE ]


*** General information:
* Speakers name or alias (IRC/Jabber/...)
* Demo [Y/N]
* Number of lines of code written during your project ?
* I need help with visas [Y/N]
* Presentation Title
* Abstract
* Biography
* Email
* Website
* Address
* Phone
* Company (name) or Independent?
* additional requirements: Internet? Others?

*** Talk Format:
Please chose your talk format:
[ ] Standard (30min+10min Q&A)
  or:
[ ] Extended (80min+10min Q&A)

*** Attachments:
Specify if your submission contains any of the following information:
* Tool     [Y/N]
* Slides   [Y/N]
* Adult content [Y/N]

*** Art performance, concert format :
* Artistical intention abstract (video, picture, texte or website)
* Type of performance / concert
* Technical requirements
