## THSF(5+2)
La TOULOUSE HACKER SPACE FACTORY est un rendez-vous basé sur la mise en commun 
de connaissances, une rencontre autour de propositions artistiques, de 
conférences et d’ateliers de pratiques. L’objectif est de générer un espace 
critique et expérimental qui mette en porosité création artistique et pratiques 
des cultures hackers.
L’événement est tourné vers l’utilisation libre des technologies, leurs 
appropriations et les enjeux que cela suscite dans la transformation de nos 
sociétés.
Il est ouvert à tout-e-s, débutant-e-s, expert-e-s ou intéressé-e-s.

C’est un temps de rencontres et d’échanges, dont la formule hybride stimule la 
réflexion et la recherche dans de nombreux domaines, artistique, technologique, 
sociologique, philosophique...
2016 sera la 7 e édition de cet événement à Mix’Art Myrys Toulouse.

Pour cette 7 e édition, nous faisons le constat que l’événement s’inscrit dans 
un courant d’actions qui émergent un peu partout en Europe et dans le monde 
autour d’une forme originale d’organisation, expérimentale et évolutive. Depuis 
2010, dans la lignée du Chaos Computer Club (Berlin), ou du /tmp/lab 
(Vitry sur Seine), la THSF met en relation sur quatre jours, plusieurs 
hackerspaces invités à se rencontrer, échanger, créer ensemble.
Le temps de travail que constitue l’événement, est organisé par et pour les 
acteurs eux mêmes.
Tout en s’inscrivant dans cette lignée, la particularité de la Toulouse 
HackerSpace Factory est de s’inscrire dans un collectif d’artistes.

## REVUE DE PRESSE
http://www.makery.info/2015/05/19/les-hackers-a-la-rencontre-du-grand-public-au-thsf-de-toulouse

http://www.liberation.fr/futurs/2015/05/15/a-toulouse-les-hackers-surfent-sur-le-canal-du-midi_1310194

## AXE DE RECHERCHE / INTENTIONS
C’est le « TEMPS » et la polysémie de cette notion qui guidera cette 7ème édition :
Le temps est un élément fondamental de la vie culturelle, sociale et personnelle
des individus, agissant comme un référentiel dans nos sociétés contemporaines. 
Il nous renvoie à l’idée de début, de naissance, de vecteur, de dynamique, de 
cycle, de fin, de mort.
Un certains nombre de sujets d’actualité essentiels (loi de programmation 
militaire, révélations Snowden, logiciels libres, open data...) ont été 
solutionnés d’une manière peu satisfaisante ces derniers temps. Un sentiment 
d’abattement à été assez largement partagé cet été dans la communauté des geeks 
et des hackers, et plus largement encore en cette fin d’année 2015.
(we lost the war 32c3 : https://media.ccc.de/v/32c3-7501-ten_years_after_we_lost_the_war#video)

Alors ? Que faire ensuite ? Que faire maintenant ?

S’appuyer sur la question du temps pour penser l’action nous apparaît comme un 
moyen de revenir aux fondamentaux et à ce qui fait la richesse de la culture 
hackers : horizontalité, neutralité, ouverture, résilience, curiosité, amour.
Avec la volonté qu’au delà des personnes, au delà du groupe et de l’événement 
lui même, ces idées continueront d’agir sur des temps longs et sauront se 
perpétuer malgré les aléas du contexte.
Dans cette édition de THSF, nous mettrons à plat la notion de temporalité, 
qu’elle soit :
* individuelle et biologique (psychologie, quotidien),
* politique (deadline de la dette grecque, agora 500 av JC),
* médiatique (temps de cerveau disponible),
* guerrière (vitesse de l’info, temps réel des drones),
* financière (Hyper Frequency Trading),
* artistique (temps réel),
* informatique (0 day),
* physique (la variable temps)

Et à partir de ces différents champs, et dans un esprit «hacking» (démontage, 
déconstruction, détournement, ré-appropriation...) les questions proposées aux 
artistes et intervenants invités pourront être les suivantes :
* Comment se forment une information, un message, une symbolique ?
* Comment le temps d’une idée est-il différent du temps de l’homme ?
* Quelle est la place de l’artiste ?
* Le temps est il capitalisable ? Peut on l’économiser ? Comment sortir du 
paradigme « time is money » ?
* Comment réaliser un internet des esprits ?

## CORRÉLATION ENTRE LE PROPOS A DÉPLOYER ET LA FORME ENVISAGÉE
Les espaces de création/monstration du collectif Mix’Art Myrys, qui se 
développent sur 4 000m2 de halle (ateliers de création, salles de 
répétitions/diffusion, exposition...), seront organisés sur le modèle des 
dynamiques d’aménagements urbains organiques et donc multi fonctionnels, 
d’aménagements poétiques incluant la notion de nomadisme et/ou dérive selon 
Guy Debord. Dans cet espace refactorisé, chaque hackerspace et chaque artiste 
habitera un espace à la fois ouvert sur les autres, permettant l’interaction, 
et autonome, lui offrant son rythme propre.
Au delà des espaces in situ, des partenariats sont en cours d’élaboration avec 
Lieu Commun (Espace d’Art Contemporain), les Ateliers TA (espace de production 
art contemporain) et le Centre Culturel Bellegarde.
Nous nous adressons à deux types de publics :
- Les spectateurs, conviés à des horaires définis.
- Les participants, proposant une action en lien avec la ligne éditoriale, ayant
un accès permanent aux espaces et la possibilité d’une participation continue.
Un protocole de «résidence hackers» des collectifs participants (hackerspaces et
équipes artistiques) sera mis en place.
Au delà d’une programmation continue et arrêtée en amont, ce principe H24 
donnera lieu, sur les quatre jours, à la mise en place de performances 
artistiques ponctuelles, répétitives ou accidentelles, ou se propageant sur la 
durée de l’événement. Il permettra de mêler différentes temporalités, tout en 
travaillant sur le flux (stream, retransmission en direct, visio-conférences...).

PISTES DE DÉVELOPPEMENT DE L’ÉCRITURE
L’écriture se développera selon trois axes :
- une co-curation de l'évènement par plusieurs artistes (Benjamin Cadon, 
Pierre Mersadier)
Nous avons élaboré ensemble une première liste d’artistes invités à participer.

- La programmation artistique se fondera également sur un appel à projets qui 
reprendra les principes de la note d’intention générale de l’événement. Il est 
ouvert à toutes disciplines et les réponses seront étudiées par un comité 
artistique et scientifique (Joël Lécussan, Sophie de Angelis, Herveline 
Guervilly, Lionel Delteil, Marc Bruyere, Florent Galès, Frédéric 
Villeneuve-Séguier, Thierry Boudet , Laurent Guerby, Clément Amira, Perlin 
Darcissac, Thomas Bigot ).

- Le programme ainsi défini, sera édité et communiqué via une grille de 
programme multipistes à trous, reprenant les actions retenues et laissant des 
plages vacantes. Nous permettons ainsi à tout artiste de pouvoir soumettre une 
proposition pendant la durée de l’événement et jusqu’au dernier jour (formulaire
en ligne ou papier).

## COMMISSARIAT ET PISTES DE PROGRAMMATION (en cours)
## ARTISTES ASSOCIÉS à la direction artistique

Pierre Mersadier - Artiste plasticien

Benjamin Cadon - Artiste associé

## ARTISTES :
Marco Donnamura (Londres, performance) - http://marcodonnarumma.com

Julian Oliver (Berlin, performance et installation) - http://julianoliver.com

Scott Draves (New York, performance et atelier)

Jerome Abel (Marseille, performance) - http://jeromeabel.net

Hp Process (Databaz, Angoulème, performance) - http://databaz.org

CHDH (Paris, performance) - http://www.chdh.net

V-Atak (Paris, performance) - http://www.v-atak.com

Zden/Satori, (Slovaquie, demoscene) - http://www.satori.sk

Nibul (Toulouse, performances sonores) - http://nibul.bandcamp.com

Toys’r’noise (Lille, performances sonores) - http://toysrnoise.free.fr/wp

Pierre Gordeeff (Nantes, performances sonores) - http://pierregordeeff.com

Coralie Gourguechon (Toulouse, Exposition, atelier) - http://coraliegourguechon.fr

A4Putevie et <++ (Toulouse, Exposition et performance)

## COLLECTIFS :
Otolab (Milan, Italie) - http://www.otolab.net/

Pixelache.ac : Piksel (Bergen, Norvège) - http://pixelache.ac/

Rybn et PI-Node (Paris) - http://p-node.org/ http://rybn.org/

Labomedia (Orléans) - http://labomedia.org/

Apo33 (Nantes) - http://apo33.org/

Planete Laboratoire (Paris) - http://laboratoryplanet.org

Echelle Inconnue (Rouen) - http://www.echelleinconnue.net/

Metallu à chahuter (Lille) - http://metaluachahuter.com/

Vision’R (Paris) - http://www.vision-r.org/

## HACKERSPACES :
ALICE Lab (Perpignan) - http://www.alicelab.fr/

ElectroLab (Nanterre) - http://www.electrolab.fr/

Le Jack (ex LeLoop - Paris) - http://leloop.org/

LabX (Bordeaux) - http://www.labx.fr/

Labdispak - http://www.labdispak.fr/

Lebib (Montpellier) - https://lebib.org/

PAulla (Pau) - https://www.paulla.asso.fr/

tmp/lab (Choisy) - http://www.tmplab.org/

Breizh Entropy (Rennes) - http://www.breizh-entropy.org/

## PRESSE/PROFESSIONNELS
http://www.mediapart.fr

http://www.liberation.fr

http://korben.info

http://makery.fr

Illusion & Macadam (coopérative)

## CONFÉRENCIERS
Jérémmie Zimmermann (Ex-Quadrature du net)

Benjamin Bayard (French Data Network)

Jacob Applebaum http://fr.wikipedia.org/wiki/Jacob_Appelbaum

Andy Müller-Maguhn https://fr.wikipedia.org/Andy_MBCller-Maguhn

George Danezis http://www0.cs.ucl.ac.uk/staff/G.Danezis/

Joanna Rutkowska : https://en.wikipedia.org/Joanna_Rutkowska

Julian Assange : https://wikileaks.org/

Roger Dingledine http://www.freehaven.net/~arma/cv.html

Reflets.Info (Qosmos, Amesys, ...)

Laurent Chemla (Caliopen)

Iohannes M Zmoelnig (Puredata)

Edmond Couchot (Images et temps)

Paul Virilio (Vitesse De L’information)

Bernard Steigler (Retour d’expérience)

Jean Claude Michea (Orwell)

Etienne Klein (Le «Temps») https://www.youtube.com/watch?v=NDYIdBMLQR0

Bernard Friot (revenu de base),

Jaromil (freecoin, etc : https://jaromil.dyne.org/)

pouet.net (demoscene)



## LIEUX PARTENAIRES :
Lieu Commun : http://www.lieu-commun.fr/wait/

Ateliers TA : https://atelierta.wordpress.com/

Centre Culturel Bellegarde
