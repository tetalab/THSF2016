## From https://wiki.hackerspaces.org/France

----------------------------------------------------------------------------------
* LOL - Laboratoire Ouvert Lyonnais (Lyon) - root@LaboLyon.fr
* Breizh Entropy (Rennes) - contact@breizh-entropy.org
* Jeanne D'Hack (Rouen) - info@jeannedhack.org
* META (Blois) - cc.meta@riseup.net
* ALICELab (Perpignan) - stephane@alicelab.fr
* LOG - Laboratoire Ouvert Grenoblois (Grenoble) - ML - grenoble-hackerspace@listes.logre.eu
* Le BIB (Montpellier) - envoi par webform
* Electrolab (Nanterre) - ML - electrolab@electrolab.fr
* /tmp/lab (Choisy le Roi) - contact@tmplab.org / ML - tmplab@lists.tmplab.org
* Labomedia (Orléans) - contact@labomedia.net & bcadon@labomedia.net
* L@Bx (Bordeaux) - ML - mailing@labx.fr
* La Paillasse (Paris) - event@lapaillasse.org
* BlackBoxe (Paris) - contact@blackboxe.org
* Laboratoire Ouvert Ardèche-Drôme (Valence) - contactl0ad@l0ad.org
* Hackerspace Angers (Angers) - hackerspaceangers@riseup.net
* Hackstub (Strasbourg) - ML - discussions@lists.hackstub.netlib.re
* Le Loop (Paris) - ML - leloop@lists.leloop.org 
* Paulla (Pau) - antoineve@paulla.asso.fr
    
----------------------------------------------------------------------------------
Mail explicatif:

Subject: THSF2016 - "résidence hacker" - appel à participation


Bonjour à vous toutes et tous,

Un petit message avec un complément d'information concernant plus particulièrement les hackerspaces dans le cadre de l'appel à participation du THSF 2016. (CFP: http://tetalab.org/fr/thsf/call-for-paper)

Les hackerspaces sont invités à participer de plusieurs manières, sans restrictions sur le nombre de propositions :
- ateliers ouverts au public 
- stands de présentation/démo
- performances
- conférences
- résidences hacker 

Pour ce THSF, une salle de 250m² sera en mode "résidence hacker" afin de donner la possibilité aux hackerspaces de venir travailler en mode 24h/24h pendant les 4 jours du festival. 

L'espace résidence sera organisé pour accueillir et permettre à plusieurs petits groupes de personnes de se retrouver pour travailler sur un projet en cours ou improvisé pour l’événement. DIY, code, arduino, bricole, installation artistique...
La salle sera ouverte du début à la fin du festival pour les participants à la résidence, et l'accès au public sera réduit afin de ne pas trop déranger. 

Coté vie sur place, il y aura de quoi manger et boire pas cher, des coins canapés et dortoir pour faire une petite sieste, et le reste de l’événement pour se changer les idées.

Coté matériel, nous avons des outils, de quoi souder, une ligne haut débit, des composants élèc, de quoi bricoler, mais n'hésitez pas à amener laptops et matériel spécifique selon vos besoins.

Si vous êtes intéressés pour participer à la résidence hacker pendant le THSF, envoyez un mail à contact@tetalab.org avec en sujet "[thsf: résidence hacker] proposition -bla- ".

Dans le mail, n'oubliez pas de préciser le nombre de personnes et les besoins techniques (table, élèc, connexion, etc).


Bonne soirée à vous, en espérant vous voir pour le THSF ! 

frtk@tetalab


----------------------------------------------------------------------------------

* HZV (Saint Ouen, State Seine Saint Denis)
    * Website : https://www.hackerzvoice.net
    * Contact : communication@hackerzvoice.net
* ~~HacKnowledge (Rennes)~~ - Site Wordpress & off topic
    * Website http://www.hacknowledge.org
    * Contact : *null*
* LOL - Laboratoire Ouvert Lyonnais (Lyon)
    * Website : http://LaboLyon.fr
    * Contact : root@LaboLyon.fr
* ~~NYBI.CC (Nancy, State Lorraine)~~ - FabLab/Makerspace
    * Website : http://nybi.cc
    * Contact : *null* || association@nybi.cc
* ~~PiNG (Nantes, State Pays de la Loire)~~ - Fablab (supa)
    * Website : http://www.pingbase.net/
    * Contact : *null*
* ~~Le Garage (Saint-Étienne)~~ - Site Down
    * Website : http://www.legarage-gpl.org
    * Contact : *null*
* ~~Tyfab (Brest)~~ - Fablab
    * Website : http://tyfab.fr
    * Contact : *null*
* ~~NaoHack (Nantes)~~ - Site Down
    * Website : http://naohack.org/
    * Contact : *null* 
* ~~Osilab (Lille)~~ - Site en japonais
    * Website : http://www.osilab.org 
    * Contact : *null*
* Breizh Entropy (Rennes)
    * Website : http://breizh-entropy.org/
    * Contact : contact@breizh-entropy.org
* Jeanne D'Hack (Rouen)
    * Website : http://www.jeannedhack.org/
    * Contact : info@jeannedhack.org
* ~~PhenixIT (Melun)~~ - Site Down 
    * Website http://www.phenixit.net
    * *null*
* L'abscisse (Dijon) - hacklab/fablab sans subventions - ?
    * Website : http://www.coagul.org
    * Contact : c-bureau@outils.coagul.org
* META (Blois) - hack urbain ?
    * Website : https://cc-meta.cc/
    * Contact : cc.meta@riseup.net
* ~~Corrèze Elab (Meyssac)~~ - pas d'info
    * Website : *null*
    * Contact : *null*
* ~~Artlab (Paris)~~ - Makerspace - ?
    * Website : http://www.digitalarti.com/artlab
    * Contact : info@digitalarti.com
* Labomedia (Orléans)
    * Website : http://labomedia.org
    * Contact : contact@labomedia.net & bcadon@labomedia.net
* Fabelier (Paris) - Site Wordress - oriente science - corporate ?
    * Website : http://fabelier.org)
    * Contact : google group - https://groups.google.com/forum/#!forum/fabelier
* HackEns (Paris) - ENS 
    * Website : http://hackens.org/
    * Contact : hackens-membres@ens.fr
* L@Bx (Bordeaux) - Attente d'approbation pour acces mailing list
    * Website : https://www.labx.fr
    * Contact : email Mathias a recuperer
* La Paillasse (Paris)
    * Website : http://www.lapaillasse.org/
    * Contact : event@lapaillasse.org
* BlackBoxe (Paris) - Contact sur inscription ML
    * Website : http://blackboxe.org/
    * Contact : *null*
* Sainté openlab (Saint-Étienne) - Pas de contact
    * Website : https://www.steopenlab.eu/
    * Contact : *null*
* Laboratoire d'Aix-périmentation et de Bidouille (Aix-en-Provence) - Formulaire de contact
    * Website : http://www.labaixbidouille.com/
    * Contact : *null*
* Hackers-Lab (Sartène) - Pas de contact
    * Website : http://www.hackers-lab.org/index.php?title=Main_Page
    * Contact : *null*
* PMClab (Paris) - Pas de contact
    * Website : http://pmclab.fr
    * Contact : *null* 
* HackGyver (Belfort) -
    * Website : http://www.hackgyver.fr
    * Contact : contact@hackgyver.org
* Electrolab (Nanterre)
    * Website : http://www.electrolab.fr
    * Contact : contact@electrolab.fr
* /tmp/lab (Choisy le Roi)
    * Website : http://www.tmplab.org/
    * Contact : contact@tmplab.org
* HAUM (Le Mans)
    * Website : http://haum.org
    * Contact : contact@haum.org
* EmancipoLab (Paris)
    * Website : *null*
    * Contact : emancipolab@gmail.com
* Laboratoire Ouvert Ardèche-Drôme (Valence)
    * Website : http://l0ad.org
    * Contact : contactl0ad@l0ad.org
* Technistub (Mulhouse) - Fablab & Makerspace
    * Website : http://www.technistub.org
    * Contact : martial@technistub.org (president)
* Hackerspace Angers (Angers)
    * Website : http://hackerspaceangers.noblogs.org/
    * Contact : hackerspaceangers@riseup.net
* Le BIB (Montpellier)
    * Website : http://lebib.org
    * Contact : PG est sur la mailing list (Contact direct)
* Nicelab (Nice)
    * Website : https://nicelab.org
    * Contact : 
* Sunlab (Viroflay) 
    * Website : http://mysunlab.org/
    * Contact : 
* MDesign (Metz)
    * Website : http://mdesign.fr
    * Contact : mail@ecofablab.eu
* LABio (Senlis)
    * Website : http://www.asso-labio.fr/
    * Contact :  asso.labio@gmail.com
* Hackstub (City Strasbourg, State Alsace, 
    * Website : https://hackstub.netlib.re/
    * Contact : mailing list
* LOG - Laboratoire Ouvert Grenoblois (Grenoble)
    * Website : https://logre.eu
    * Contact : mailing list 
* ALICELab (Perpignan)
    * Website : http://alicelab.fr/
    * Contact : stephane@alicelab.fr
* SmartLab aka dumbLab (Limoges)
    * Website : http://www.smartlab.re
    * Contact : smartlab.limoges@gmail.com


----------------------------------------------------------------------------------
Modele du mail envoyé (vrx):

TOULOUSE HACKER SPACE FACTORY 2016 - Appel à participation
19-22 Mai 2016, Mixart`Myrys (Toulouse, France)

Salut à vous toutes & tous !

En lien ci dessous un appel à participation pour les hackers,
performeurs, makers, libre penseurs, artistes à l’occasion de la 7eme
édition du THSF à Toulouse.

Car nous voulons un événement porté par la communauté pour la communauté.

http://tetalab.org/fr/thsf/call-for-paper

La date limite de dépôt des propositions est fixée au 10/04/2016.

Merci de partager l'info à vos membres !

Vrx

http://tetalab.org
