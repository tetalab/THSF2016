### *TOULOUSE HACKER SPACE FACTORY 2016 - Appel a participation*
### 19-22 Mai 2016, Mixart`Myrys (Toulouse, France) 

Ceci est un appel à participation à tous les hackers, performeurs, 
makers, libre penseurs, artistes...

La date limite de dépôt des propositions est fixée au 10/04/2016.

#### À PROPOS DU THSF
La TOULOUSE HACKER SPACE FACTORY est un rendez-vous basé sur la mise en
commun  de connaissances, une rencontre autour de propositions
artistiques, de conférences et d’ateliers. L’objectif est de générer un 
espace critique et expérimental.

L’événement est tourné vers l’utilisation libre des technologies, leurs 
appropriations et les enjeux que cela suscite dans la transformation de 
nos sociétés.
Il est ouvert à tout-e-s, débutant-e-s, expert-e-s ou intéressé-e-s.

C’est un temps de rencontres et d’échanges, dont la formule hybride 
stimule la réflexion et la recherche dans de nombreux domaines, 
artistique, technologique, sociologique, philosophique...

#### THSF7
Du 19 au 22 mai 2016 aura lieu la 7eme édition de cet événement à Mix’Art Myrys 
(Toulouse).

Un certain nombre de sujets essentiels (loi de 
programmation  militaire, révélations Snowden, logiciels libres, 
open data, état d'urgence...) ont été solutionnés d’une manière peu satisfaisante ces 
derniers temps. Un sentiment d’abattement a été assez largement partagé 
cet été dans la communauté hackers, et plus largement encore en cette 
fin d’année 2015.
Alors, que faire ensuite ? Que faire maintenant ?

S'appuyer sur la notion de temporalité et de temps long des idées pour 
penser l'action nous apparaît efficace quand notre volonté est que, au 
delà des personnes, du groupe et de l'événement lui même, ces 
idées continuent d'agir et de se perpétuer malgré les aléas du contexte.

Cette année, nous allons donc mettre à plat la notion de temporalité, 
qu’elle soit :

* individuelle et biologique (psychologie, quotidien, santé),
* politique (deadline de la dette grecque, agora 500 av JC),
* médiatique (temps de cerveau disponible, vitesse de l'info),
* guerrière (vitesse de l’info, temps réel des drones),
* financière (Trading Haute Fréquence),
* artistique (temps réel, évènement éphémère),
* informatique (0 day),
* physique (la variable temps)
* humaine (à quelle heure l'apéro ?)


#### MISE EN PLACE
Nous disposons : 
- d'un hangar de 4000m2,
- d'espaces pour des ateliers,
- d'une salle de conférence, 
- d'une salle de concert,
- d'un salle 250m2 en mode "résidence" hacking,
- de stream video et audio,
- de coins et recoins pour les installations,
- d'un ou plusieurs bars,
- de restauration sur place pas chere,
- d'un tuyau vers les internetz d'environ 8Gb/s,
- d'un parking

Le lieu sera ouvert 24h sur 24 pendant les 4 jours de l'évènement pour 
tous les participant(e)s.


#### TRANSPORT ET HEBERGEMENT
Les frais de transport et l'herbergement (sur place ou défraiement) sont pris en 
charge. Ne pas oublier de preciser les details du trajet et des besoins en 
hebergement lors de la reponse au CFP.
La nourriture et la boisson seront pris en charge le jour de votre intervention.

#### DATES :
Deadline CFP : 10 Avril 2016

Evenement: du 19 au 22 Mai 2016

#### CONTACTS
Les reponses au CFP doivent etre envoyées a l'adressee : contact@tetalab.org

#### POUR FINIR 
Nous pensons que la diversité contribue fortement à la richesse de 
l'événement. 

Nous espérons vous voir contribuer, nous voulons un événement porté par 
la communauté pour la communauté sans la moindre attente commerciale 
(ce qui ne veut pas dire que les bières seront gratuites).

##### Le THSF vous est proposé par le Tetalab, Tetaneutral & Mix'Art Myrys.

#### REVUE DE PRESSE EDITION 2015
http://www.makery.info/2015/05/19/les-hackers-a-la-rencontre-du-grand-public-au-thsf-de-toulouse

http://www.liberation.fr/futurs/2015/05/15/a-toulouse-les-hackers-surfent-sur-le-canal-du-midi_1310194
