Du 29 mai au 1er juin 2014
Toulouse, France

--- Here we go again ---
===========><===========
--> Iterate, convolve <--
-->   break, rebuild  <--

Le THSF est une rencontre de 4 jours/4 nuits autour :
des contemplations, du code, du partage.
c'est une rencontre qui engage à une utilisation créative des technologies.

Ceci est un appel à participation à tous les hackers, performeurs, makers, libre penseurs, artistes...

Nous pensons que la diversité contribue fortement à la richesse de l'évément. Plus bas, une liste non exhaustive des sujets que nous souhaiterions aborder :

    Trucs fabriqués en 3D, Machines faites maison
    Installations tangibles, interactives, arts numériques
    Systémes Audio/Video, séquenceurs, performances live.
    Démos 2n bits audio, Musique algorithmique et Ibnizienne
    Couture, Brassage maison, Nourriture bio, Chimie artisanale

    Logiciels, données et matériels libres, polémiques anti brevets
    Liberté, Droit des citoyens, Démocratie, Résilience,
    é‰conomie numérique, systémes monétaires décentralisés, Communication P2P, algo trading
    Neutralité du réseau, fournisseurs d'accés alternatifs,

    Bizarreries électroniques, Leds qui clignotent, robots malades, courts circuits, GIF animés
    Technologie du réseau, Systémes d'exploitations, exploitations des systémes, sécurité, désosage, bugs et fonctionnalités.
    Ordinosaures, Amstrad, Commodore, Bornes d'arcade, futur et retro-gaming

    Théé¢tre Augmenté, cirque numérique, chorégraphie digitale, événements subversifs (mélangez les termes comme vous le souhaitez..).
    Electronic, noise, expérimental, circuit-bending saturé, disco (s)hits.

Nous disposons : d'espace pour les ateliers, d'une salle de conférence, d'une scéne de concert, de stream video, de coins et recoins pour les installations, pour les nerds...
d'un ou plusieur bars (galactique ou non :)) et d'un tuyau vers le monde des intertubes d'environ 8Gb/s.

Nous espérons vous voir contribuer, nous voulons un événement porté par la communauté pour la communauté sans attentes commerciales.

La date limite de dépôt des propositions est fixé au 02/03/2014

Le THSF vous est proposé par le Tetalab, Tetaneutral & Mix'Art Myrys.

Pas de 733t ou de n00bZ ici, nous sommes tous ici pour apprendre, pratiquer et s'amuser...
May 29 to June 1st 2014
Toulouse, France

--- Here we go again ---
===========><===========
--> Iterate, convolve <--
-->   break, rebuild  <--

THSF is a four days long meeting (night and day) about:
contemplate, code, share, meet people from community and get all fun we can with technologies.

This is a call for participation to all hackers, performers, makers, social thinkers, artists...

We're looking for quiet a lot of differents topics ranging from :

    3D Making stuffs, DIY machinery
    Tangible, interactive installation setup, digital art
    Audio/Video framework, sequencing, live setup.
    2n bits audio/demo, Ibniz and algorithmic music
    Knitting, Brewing, Bio food, artisanal chemistry

    Open source software, open hardware, open data, anti patent trolls
    Liberty, citizen rights, reclaim democraty, resilience
    Electronic economy, decentralized money system, P2P communication, algo trading
    network neutrality, DIY ISP

    Electronic weirdness, candy blinken light, buggy robotics, short-circuits, animated GIFs
    Network technology, operating system, security, reverse engineering, bugs and features.
    Dino OSes, Amstrad, Commodore, arcade cabinets, past and future entertainment systems

    Augmented theater, numeric circus, digital choregraphy, subversive happenings (please mix n-uplets as you wish ...).
    Electronica, noise, experimental, glitchy circuit bended music, disco (s)hits.

We'll provide : rooms ( well .. kind of ... ) for workshops, conference hall, concert scene, video stream, corners for casual installation,
spot for nerds, bars and about 8GB network connection.

We're really looking forward to __your__ contribution, as we expect a true community driven event without any commercial expectation.

The deadline for submissions is 02/03/2014

THSF is brought to you by Tetalab, Tetaneutral & Mix'Art Myrys.

No el8 nor n00bZ here as you/we all still need to learn, practice, and have fun ...

http://www.thsf.net

http://blog.thsf.net/page/Conferences

http://lists.tetalab.org/listinfo/thsf-2014
